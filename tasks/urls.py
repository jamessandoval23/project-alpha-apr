from django.urls import path
from . import views

urlpatterns = [path("mine/", views.show_my_tasks, name="show_my_tasks")]
