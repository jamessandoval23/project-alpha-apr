from django import forms


# Create your models here.
class Login(forms.CharField):
    username = forms.CharField(max_length=150)
    password = forms.CharField(widget=forms.PasswordInput, max_length=150)
