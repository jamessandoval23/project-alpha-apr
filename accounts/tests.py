from django.test import TestCase, client
from django.urls import reverse
from django.contrib.auth.models import User
from .models import Project



# Create your tests here.
class ProjectListViewTests(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.project1 = Project.objects.create(name='Project 1', owner=self.user)
        self.project2 = Project.objects.create(name='Project 2', owner=self.user)self.project3 = Project.objects.create(name='Project 3')
        self.client = (client)
        self.client.login(username='testuser', password='testpassword')



    def test_project_list_view_accessible(self):
        url = reverse('project_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302)


    def test_project_list_view_only_shows_user_projects(self):
        url = reverse('project_list')
        response = self.client.get(url)
        self.assertQuerysetEqual(
            response.context['projects'],
            [self.project1, self.project2],
            transform=lambda x: x
        )

    def test_project_list_view_not_accessible_for_anonymous_user(self):
        # Logout the user for this test
        self.client.logout()
        url = reverse('project_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/accounts/login/?next=/projects/')

class LogoutViewTests(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username="testuser", passwords="testpassword")
        self.client = client()
        self.client = client()
        self.client.login(username = "testuser", password= "testpassword")

    def test_logout_redirects_to_login_page(self):
        response = self.client.get(reverse("logout"))
        self.assertRedirects(response, reverse("login"))
